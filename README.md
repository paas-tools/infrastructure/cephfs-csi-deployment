# CephFS CSI deployment

This project deploys a cephfs-csi driver in OpenShift.
The Cephfs-csi driver takes care of provision CephFS PersistentVolumes objects based on incoming PersistentVolumeClaims.
The CSI CephFS deployment integrates the following components:

- Manila CSI provisioner, in charge of creating/deleting CephFS shares via the OpenStack Manila API (creates PVs from user PVCs)
- CephFS CSI driver itself is in charge of mounting the CephFS PVs on nodes as required by applications
- Storageclass definitions, which expose the ability to request CephFS PVCs to users
- CephFS Volume Backup With Restic. We use [restic](https://github.com/restic/restic) as recommended by `IT/ST` for backing up OpenShift application volumes on `CephFS`.

Info in [deploy-cephfs](https://github.com/ceph/ceph-csi/blob/master/docs/deploy-cephfs.md)
and in here, [cloud infra docs](https://github.com/kubernetes/cloud-provider-openstack/blob/master/docs/using-manila-provisioner.md#authentication-with-manila-v2-client)

## Deployment details

The cephfs-csi driver is deployed with Helm in each OpenShift environment, in the `paas-infra-cephfs` namespace.
The various OpenShift environments are defined in [.gitlab-ci.yml](.gitlab-ci.yml).

### Permission initialization for new volumes

We need this permission initialization for CephFS volumes because the volumes provisioned by Manila only give write access to `root`, while OpenShift apps run
as `non-root` users. So with this, we make sure that an OpenShift application will be able to write to `CephFS` volumes.

This [`init-permissions` controller](https://gitlab.cern.ch/paas-tools/storage/init-permission-cephfs-volumes) watches for persistent volume events. When a PV is created,
if its storageClass has an annotation `init-permission-volumes.cern.ch/init-permission=true`, the controller grants permissions to it. This happens by launching a
[Job](https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/) that mounts the CephFS volume into a pod and
[runs `chown`/`chmod` on the volume's root](https://its.cern.ch/jira/browse/CIPAAS-543). Once this is done, the volume is unmounted
and the Job deleted. The application that requested that volume can now mount the volume and write to it with its Openshift-assigned non-root user.

### Reclaim CephFS volumes after a grace period

By default, the `persistentVolumeReclaimPolicy` of a CephFS PV creation is set to `Retain`, this means that even when the user deletes this PV,
it will persist in our infrastructure in order to recover PVs unfortunately removed by mistake.

In order to delete PVs marked for deletion, we have created a cronjob in charge of deleting permanently PVs marked for deletion
in a specific period of time, this `time-reclaim` is set in each of the StorageClasses [CIPAAS-542](https://its.cern.ch/jira/browse/CIPAAS-542)

Once this time-reclaim is reached, the cronJob patches the `spec` of the PV and sets the `persistentVolumeReclaimPolicy` to `Delete`,
this will trigger a permanently deletion of the PV.

### Backup CephFS persistent volumes

We automatically backup any PV created in the default `cephfs` StorageClass. On the other hand, we do **NOT** provide backups for the `cephfs-no-backup` StorageClass.
It is not possible to recover a previous state of a volume created in the `cephfs-no-backup` StorageClass. This is appropriate for non-critical,
frequently-changing or rolling data. In any case, the PVs will retain several days before the PV reclaim. This value can be seen in
`reclaimDeletedVolumes.gracePeriodBeforeDeletingReleasedPV` in `values.yaml` of this project.

All the information about this component can be found in [backup-cephfs-volumes](https://gitlab.cern.ch/paas-tools/storage/backup-cephfs-volumes#backup-solution-for-cephfs-persistent-volumes)

There is also a procedure for restoring data from backups, you can find this doc in [openshiftdocs](https://openshiftdocs.web.cern.ch/openshiftdocs/Operations/Storage/recoverDataFromResticBackup/)

## Deploying to a new environment/cluster

### Prerequisites

#### SELinux

NB: the [`container_use_cephfs` SELinux boolean](https://bugzilla.redhat.com/show_bug.cgi?id=1692369) must be
enabled on all OpenShift nodes for the containers to have access to CephFS mounts.

#### Generate restic repo password

A random password must be created for each environment. The backups will be unusable without the password.
This is a design decision of [restic](https://github.com/restic/restic).

Generate a random password with e.g. `cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`

The secret must go in cephfs-csi helm deployment `HELM_VALUES_<ENV>` of this repository (described later in this document) together with other secrets support.
The Helm deployment will create a Kubernetes secret containing the password.
The password will then be passed to the `restic` pods' env vars.

#### Initialize S3 bucket for restic backup

We back up to S3, to a separate S3 bucket per environment.
See [the list of S3 buckets](https://openshiftdocs.web.cern.ch/openshiftdocs/Deployment/List_Of_Resources_For_Project/#s3-buckets)
for how to create them for each environment.

We need to initialize each of the buckets **only** once, use a `restic` container and set environment variables from gitlab secrets:

To do this, we can do it locally with our [restic client](https://restic.readthedocs.io/en/latest/020_installation.html) installed:

```
export AWS_ACCESS_KEY_ID=`AWS_ACCESS_KEY_ID`
export AWS_SECRET_ACCESS_KEY=`AWS_SECRET_ACCESS_KEY`

restic -r s3:s3.cern.ch/<DESIRED_BUCKET_NAME>/restic-backup init
# enter password for new repository: `RESTIC_PASSWORD`
# enter password again: `RESTIC_PASSWORD`
# created restic repository 6caf4cef62 at s3:s3.cern.ch/<DESIRED_BUCKET_NAME>/restic-backup
```

If everything went well, in [Openstack](https://openstack.cern.ch) -> container -> <DESIRED_BUCKET> you will be able to see a folder called `restic-backup`

#### Prepare cluster for Helm deployment

Some preparation steps need to be performed with cluster-admin permissions.
This needs to be done only once per OpenShift cluster/environment.

Start a Docker container for installation (mounting this git repo in /project):

```bash
docker run --rm -it -v $(pwd):/project -e TILLER_NAMESPACE=paas-infra-cephfs gitlab-registry.cern.ch/paas-tools/openshift-client:v3.11.0 bash
```

Then in that container:

```bash
# login as cluster admin to the destination cluster
oc login https://openshift-test.cern.ch #or openshift-dev.cern.ch or openshift.cern.ch
export HELM_VERSION=2.13.1 # make sure to use the same Helm version found in .gitlab-ci.yml
curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
mv --force linux-amd64/{helm,tiller} /usr/bin/
# create namespace and serviceaccounts for deployment
helm template /project/prerequisites/ | oc create -f -
# obtain the token for the Tiller service account
oc serviceaccounts get-token -n paas-infra-cephfs tiller
```

The rest of the deployment will be handled by GitLab CI.

#### Store secrets in CI/CD and OpenShift secrets

We need to create 2 [GitLab CI variables](https://docs.gitlab.com/ee/ci/variables/#via-the-ui) in this project
for each OpenShift environment (see environment definitions in [.gitlab-ci.yml](.gitlab-ci.yml) for the exact
variable names for each env):

1. a variable `TILLER_TOKEN_<ENV>` with the token for the `tiller` service account obtained above
2. a variable `HELM_VALUES_<ENV>`, in the form of a Helm values file as follows:

```
openstack:
  userName: <username>
  password: <password>
  projectID: <projectID>
cephfs-backup-pvs:
  s3:
    cephfsBackupS3AccessKey: wwwww
    cephfsBackupS3SecretKey: xxxxx
    cephfsBackupPassword: yyyy
    cephfsBackupRepositoryBase: s3:https://s3.cern.ch/<DESIRED_BUCKET_NAME>/cephfs-pv-backup
```

The `openstack` section specifies the OpenStack project to be used by that OpenShift environment to
provision Manila CephFS shares, and the credentials to use.
Refer to [the Openshift resources list](https://openshiftdocs.web.cern.ch/openshiftdocs/Deployment/List_Of_Resources_For_Project/#paas-manila-).

The S3 access/secret keys are [obtained when creating the S3 buckets](https://openshiftdocs.web.cern.ch/openshiftdocs/Operations/Storage/createS3Buckets/#obtain-s3-credentials).

The `cephfsBackupPassword` is the random password generated in `Generate restic repo password` section.

The helm chart will [generate a secret](https://gitlab.cern.ch/paas-tools/infrastructure/cephfs-csi-deployment/blob/master/chart/charts/cephfs-backup-pvs/templates/cephfs-backup-secret.yaml)
from these values, which will then be consumed by the backup pods.

#### Set up test environment

NB: since we cannot currently use CSI and thus test cephfs with `oc cluster up`, we also set up a serviceaccount
_in the playground cluster only_ to test that volume provisioning/use/deprovisioning works. See details
in the test definition in [.gitlab-ci.yml](.gitlab-ci.yml). This serviceaccount's token is stored in the `APP_TEST_TOKEN`
CI variable. This can be undone when `oc cluster up` supports CSI drivers.


### Deploy the Cephfs driver

Once the GitLab CI variables have been populated, use the GitLab CI pipelines to deploy the cephfs driver components.

Running the pipeline again will update the deployment as necessary.

## Uninstall the CephFS components

Start a Docker container for installation (mounting this git repo in /project):

```bash
docker run --rm -it -v $(pwd):/project -e TILLER_NAMESPACE=paas-infra-cephfs gitlab-registry.cern.ch/paas-tools/openshift-client:v3.11.0 bash
```

Then in that container:

```bash
# login as cluster admin to the destination cluster
oc login https://openshift-test.cern.ch #or openshift-dev.cern.ch or openshift.cern.ch
# install tiller
export HELM_VERSION=2.13.1
curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
mv --force linux-amd64/{helm,tiller} /usr/bin/
# initialize local tiller
helm init --client-only
helm plugin install https://github.com/adamreese/helm-local
command -v which || yum install -y which # helm local plugin needs `which`, install if missing
helm local start
helm local status
export HELM_HOST=":44134"
# delete cephfs deployment, then prerequisites
helm del --purge cephfs
helm template /project/prerequisites | oc delete -f -
```

## Testing changes in a component's Docker image

In order to test any component of the helm charts described in this repository, follow the instructions
described in the paragraph `Using work-in-progress Docker images for dev deployment` in
[here](https://openshiftdocs.web.cern.ch/openshiftdocs/Misc/helmDevelopmentNotes/)

